from django import forms
from app import models

class capeForm(forms.Form):
    status  = forms.CharField(
            label       = 'status',
            max_length  = 300,
            widget      = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder':'How ya doin?'})    
        )
    class Meta:
        model = models.cape
        fields = ('message',)
