from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

##from django.template import Context, Template

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

from .views import home, story8, story9
from .forms import capeForm
from .models import cape

class SemogaFunctionalTest(LiveServerTestCase):

    def setUp(self):
        op = Options()
##        op.add_argument('--start-maximized')
        op.add_argument('--no-sandbox')
        op.add_argument('--headless')
        op.add_argument("--disable-dev-shm-usage")
##        op.headless = True
    ##        op.add_argument('window-size=1920x1080');
        self.selenium  = webdriver.Chrome(executable_path='./chromedriver',chrome_options=op)
        super(SemogaFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SemogaFunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        time.sleep(1)
        # find the form element
        description = selenium.find_element_by_id('id_status')
        description.send_keys("auto status")

        submit = selenium.find_element_by_name('Submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        self.assertIn("auto status", self.selenium.page_source)


        theme_butt = selenium.find_element_by_id('themec')
        theme_butt.click();

        
        body = selenium.find_element_by_css_selector('body')
        back_color = body.value_of_css_property('background-color')
        self.assertEqual(back_color, 'rgba(173, 216, 230, 1)')



class randomTest(TestCase):
    def test_url_exits(self):
        response    = Client().get('/');
        self.assertEqual(response.status_code, 200);

    def test_ini_htmlnya_bener_kaga(self):
        response    = Client().get('/');
        self.assertTemplateUsed(response, 'home.html');

    def test_viewnya_bener_kaga(self):
        
        found = resolve('/')
        self.assertEqual(found.func, home)

        
    def test_modelsnya_bener_kaga(self):
        new_mod     = cape.objects.create( status = 'cape');

        count_all_mod = cape.objects.all().count();
        self.assertEqual(count_all_mod, 1)

    def test_ini_ke_save_kaga(self):
        form = capeForm({'status':"auto test"});
        model= cape(status = form.data['status']);
        model.save();
        self.assertEqual(cape.objects.get(id=1).status, "auto test")

    def test_model_isinya_bener_kaga(self):
        hado = cape.objects.create(status="auto")
        self.assertEqual("auto", hado.__str__())

    def test_ini_beneran_html_kan(self):
        responseget = Client().get('/');
        html    = responseget.content.decode();
        self.assertTrue(html.startswith('<!DOCTYPE html>'))
        self.assertIn('<html>', html);
        self.assertTrue(html.endswith('</html>'));

    def test_datanya_beneran_keluar_kaga(self):
        
        responsepos = Client().post('/', {'status' : "katanya pepewe gampang"});
        responseget = Client().get('/');

        self.assertIn("katanya pepewe gampang", responseget.content.decode());

    def test_katanya_keluar_Ga(self):

        responseget = Client().get('/');

        self.assertIn('“Halo, apa kabar?”', responseget.content.decode());


    def test_btn_change_tema_ada_ga(self):
        responseget = Client().get('/');
        html    = responseget.content.decode();

        self.assertIn('<button onclick="themechange()" id="themec">', html)

        self.assertIn('<script type="text/javascript" src="../static/js/themechange.js">', html)
    
    def test_akordionnya_ada_ga(self):
        responseget = Client().get('/');
        html    = responseget.content.decode();
        self.assertIn('id="accordion"', html);
        self.assertIn('Aktivitas', html);
        self.assertIn('Pengalaman', html);
        self.assertIn('Prestasi', html);

    def test_ini_ajaxnya_jalan_ga(self):
        responseget = Client().get('/story8/');
        html    = responseget.content.decode();
        self.assertIn('todi', html);

    def test_ini_view_si_story8nya_bener_ga(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, story8)

    def test_ini_view_si_story9nya_bener_ga(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, story9)
        
    def test_ini_web_story9nya_200_ga(self):
        responseget = Client().get('/story9/');
        self.assertEqual(responseget.status_code, 200);
        
    def test_ini_web_loginnya_200_ga(self):
        responseget = Client().get('/accounts/login/');
        self.assertEqual(responseget.status_code, 200);
