from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from .models import cape
from .forms import capeForm

import requests

# Create your views here.
def home(request):
    html = "home.html"
    
    if request.method == "POST":
        form = capeForm(request.POST)
        if form.is_valid():
            stat = cape(
                status = form.data['status']
                )
            stat.save()
            form = capeForm
##            print(stat);
            return HttpResponseRedirect('/')
    elif request.method == "GET":
        form = capeForm()
##        print(form)

    context = {
        'status' : cape.objects.all().values(),
        'form'   : form
    }
    return render(request, html, context)


##def story8(request):
##    buk_titel   = request.GET['title']
##    url     = f"https://www.googleapis.com/books/v1/volumes?q={keyword}"
##    response= requests.get(
##        url = url
##        )
##    return JsonResponse(data = response.jason());

def story8(request):
    html = "story8.html";
    return render(request, html);

def story9(request):
    html = "story9.html";
    return render(request, html);
